package config

type Cfg struct {
	DB    *DB
	Cache *Cache
}

type DB struct {
	Driver   string
	Host     string
	Port     string
	User     string
	Password string
	Name     string
}

type Cache struct {
	Redis     string
	RedisPort string
}

func NewCache(redis string, redisPort string) *Cache {
	return &Cache{
		Redis:     redis,
		RedisPort: redisPort,
	}
}

func NewDB(driver string, host string, port string, user string, password string, dbname string) *DB {
	return &DB{
		Driver:   driver,
		Host:     host,
		Port:     port,
		User:     user,
		Password: password,
		Name:     dbname,
	}
}

func NewCfg(db *DB, cache *Cache) *Cfg {
	return &Cfg{
		DB:    db,
		Cache: cache,
	}
}
