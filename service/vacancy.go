package service

import (
	"encoding/json"
	"fmt"
	"go-parser/models"
)

type VacancyRepository interface {
	Add(vacancy models.Vacancy) error
	Get(id string) models.Vacancy
	Update(vacancy models.Vacancy) error
	Delete(id string) error
	GetAll() []models.Vacancy
	Close()
}

type VacancyService struct {
	repository VacancyRepository
}

func NewVacancyService(repository VacancyRepository) *VacancyService {
	return &VacancyService{repository: repository}
}

func (s *VacancyService) AddVacancy(json []byte) error {
	var (
		err     error
		vacancy models.Vacancy
	)
	vacancy, err = models.UnmarshalVacancy(json)
	if err != nil {
		fmt.Println(string(json))
		return err
	}
	return s.repository.Add(vacancy)
}

func (s *VacancyService) DeleteVacancy(id string) error {
	return s.repository.Delete(id)
}

func (s *VacancyService) GetVacancy(id string) ([]byte, error) {
	vacancy := s.repository.Get(id)
	res, err := (&vacancy).Marshal()
	if err != nil {
		return nil, err
	}
	return res, err
}

func (s *VacancyService) GetListVacancy() ([]byte, error) {
	res, err := json.Marshal(s.repository.GetAll())
	if err != nil {
		return nil, err
	}
	return res, nil
}
