package repo

import (
	"context"
	"fmt"
	"go-parser/config"
	"go-parser/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type VacancyMongoStorage struct {
	client     *mongo.Client
	collection *mongo.Collection
}

func NewVacancyMongoStorage(cfg config.Cfg) *VacancyMongoStorage {
	uri := fmt.Sprintf("%s://%s:%s", cfg.DB.Driver, cfg.DB.Host, cfg.DB.Port)
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))
	if err != nil {
		log.Println(err)
		return &VacancyMongoStorage{}
	}
	collection := client.Database(cfg.DB.Name).Collection("vacancy")
	return &VacancyMongoStorage{
		client:     client,
		collection: collection,
	}
}

func (v *VacancyMongoStorage) Close() {
	if err := v.client.Disconnect(context.Background()); err != nil {
		log.Println(err)
	}
}

func (v *VacancyMongoStorage) Add(vacancy models.Vacancy) error {
	var (
		vac models.Vacancy
		err error
	)
	err = v.collection.FindOne(context.Background(), bson.M{"id.vacancy_id": vacancy.Identifier.Value}).Decode(&vac)
	if err != nil {
		if fmt.Sprint(err) != "mongo: no documents in result" {
			return err
		}
		_, err = v.collection.InsertOne(context.Background(), vacancy)
		if err != nil {
			return fmt.Errorf("insert error: %s\n", err)
		}
	}
	if vac.Identifier.Value != "" {
		return fmt.Errorf("vacancy with ID:%d already exists", vacancy.Identifier.Value)
	}
	return nil
}

func (v *VacancyMongoStorage) Get(id string) models.Vacancy {
	var vacancy models.Vacancy
	err := v.collection.FindOne(context.Background(), bson.M{"id.vacancy_id": id}).Decode(&vacancy)
	if err != nil {
		log.Println(err)
	}
	return vacancy
}

func (v *VacancyMongoStorage) Update(vacancy models.Vacancy) error {
	filter := bson.D{{Key: "id.vacancy_id", Value: vacancy.Identifier.Value}}
	_, err := v.collection.ReplaceOne(context.Background(), filter, vacancy)
	return err
}

func (v *VacancyMongoStorage) Delete(id string) error {
	_, err := v.collection.DeleteOne(context.Background(), bson.M{"id.vacancy_id": id})
	return err
}

func (v *VacancyMongoStorage) GetAll() []models.Vacancy {
	findOptions := options.Find()
	var vacancies []models.Vacancy
	cur, err := v.collection.Find(context.Background(), bson.D{{}}, findOptions)
	if err != nil {
		log.Println(err)
	}
	for cur.Next(context.Background()) {
		var elem models.Vacancy
		err := cur.Decode(&elem)
		if err != nil {
			log.Println(err)
		}
		vacancies = append(vacancies, elem)
	}
	return vacancies
}
