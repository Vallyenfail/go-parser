package repo

import (
	"sync"

	"go-parser/models"
)

type VacancyStorage struct {
	data          []*models.Vacancy
	primaryKeyIDx map[string]*models.Vacancy
	sync.Mutex
}

func NewVacancyStorage() *VacancyStorage {
	return &VacancyStorage{
		data:          make([]*models.Vacancy, 0),
		primaryKeyIDx: make(map[string]*models.Vacancy),
	}
}

func (v *VacancyStorage) Add(dto models.Vacancy) error {
	//v.Lock()
	//defer v.Unlock()
	//dto.Identifier.Value = v.autoIncrementCounter
	//v.data = append(v.data, &dto)
	//v.primaryKeyIDx[dto.Identifier.Value] = &dto
	//v.autoIncrementCounter++
	//
	return nil
}

func (v *VacancyStorage) Get(id string) models.Vacancy {
	//v.Lock()
	//defer v.Unlock()
	//idInt, _ := strconv.Atoi(id)
	//if val, ok := v.primaryKeyIDx[idInt]; ok {
	//	return *val
	//}
	//
	return models.Vacancy{}
}

func (v *VacancyStorage) Update(vacancy models.Vacancy) error {
	//v.Lock()
	//defer v.Unlock()
	//_, ok := v.primaryKeyIDx[vacancy.ID]
	//if ok {
	//	v.primaryKeyIDx[vacancy.ID] = &vacancy
	//	v.data[vacancy.ID] = &vacancy
	//	return nil
	//}
	//
	//return fmt.Errorf("Vacancy with ID:%d not found\n", vacancy.ID)
	return nil
}

func (v *VacancyStorage) GetAll() []models.Vacancy {
	//var list []models.Vacancy
	//for _, v := range v.data {
	//	list = append(list, *v)
	//}
	//
	//return list
	return nil
}

func (v *VacancyStorage) Delete(id string) error {
	//idInt, _ := strconv.Atoi(id)
	//if _, ok := v.primaryKeyIDx[idInt]; ok {
	//	v.Lock()
	//	defer v.Unlock()
	//	switch {
	//	default:
	//		v.data = append(v.data[:idInt], v.data[idInt:]...)
	//	case idInt == 0:
	//		v.data = v.data[1:]
	//	case idInt == len(v.data)-1:
	//		v.data = v.data[:len(v.data)-1]
	//	}
	//	delete(v.primaryKeyIDx, idInt)
	//	return nil
	//}
	//
	//return errors.New("not found")
	return nil
}

func (v *VacancyStorage) Close() {}

// TODO: Переписать логику
