package repo

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"go-parser/config"
	"go-parser/models"
	"log"
)

type VacancyRedisStorage struct {
	sqlStore *VacancySqlStorage
	cache    *redis.Client
}

func NewVacancyRedisStorage(cfg config.Cfg) *VacancyRedisStorage {
	sqlStore := NewVacancySqlStorage(cfg)

	cache := redis.NewClient(&redis.Options{Addr: fmt.Sprintf("%s:%s", cfg.Cache.Redis, cfg.Cache.RedisPort)})

	return &VacancyRedisStorage{
		sqlStore: sqlStore,
		cache:    cache,
	}
}

func (v *VacancyRedisStorage) Add(vacancy models.Vacancy) error {
	return v.sqlStore.Add(vacancy)
}

func (v *VacancyRedisStorage) Get(id string) models.Vacancy {
	var vacancy models.Vacancy
	key := fmt.Sprintf("vacancies:%s", id)

	result, err := v.cache.Get(context.Background(), key).Bytes()
	if err == nil {
		if err := json.Unmarshal(result, &vacancy); err == nil {
			log.Println("result from cache")
			return vacancy
		}
	}
	log.Printf("cache error: %s", err)
	vacancy = v.sqlStore.Get(id)
	jsonVacancy, err := json.Marshal(vacancy)
	if err != nil {
		log.Println(err)
	} else {
		if err := v.cache.Set(context.Background(), key, jsonVacancy, 0).Err(); err != nil {
			log.Println(err)
		}
	}
	return vacancy
}

func (v *VacancyRedisStorage) Update(vacancy models.Vacancy) error {
	key := fmt.Sprintf("vacancies:%s", vacancy.Identifier.Value)
	v.cache.Del(context.Background(), key)
	return v.sqlStore.Update(vacancy)
}

func (v *VacancyRedisStorage) Delete(id string) error {
	key := fmt.Sprintf("vacancies:%s", id)
	v.cache.Del(context.Background(), key)
	return v.sqlStore.Delete(id)
}

func (v *VacancyRedisStorage) GetAll() []models.Vacancy {
	return v.sqlStore.GetAll()
}

func (v *VacancyRedisStorage) Close() {
	v.sqlStore.Close()
	if err := v.cache.Close(); err != nil {
		log.Println(err)
	}
}
