package main

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"go-parser/config"
	"go-parser/controller"
	"go-parser/repo"
	"go-parser/router"
	"go-parser/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/MadAppGang/httplog"
	"github.com/go-chi/chi"
)

func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Println("No .env file found")
	}
}

func getRepository() service.VacancyRepository {
	var repository service.VacancyRepository
	dbConf := config.NewDB(
		os.Getenv("DB_DRIVER"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME"))
	cacheConf := config.NewCache(
		os.Getenv("REDIS"),
		os.Getenv("REDIS_PORT"))
	cfg := config.NewCfg(dbConf, cacheConf)

	switch dbConf.Driver {
	case "postgres":
		repository = repo.NewVacancyRedisStorage(*cfg)
	case "mongodb":
		repository = repo.NewVacancyMongoStorage(*cfg)
	default:
		repository = repo.NewVacancyStorage()
	}
	return repository
}

func main() {
	port := ":8080"

	repository := getRepository()
	defer repository.Close()

	seleniumUri := fmt.Sprintf("http://%s:%s/wd/hub", os.Getenv("SELENIUM_ENDPOINT"), os.Getenv("SELENIUM_PORT"))

	s := service.NewVacancyService(repository)
	h := controller.NewVacancyController(s, seleniumUri)

	r := chi.NewRouter()
	//r.Use(middleware.Logger)
	r.Use(httplog.Logger)

	//vacancyController := controller.NewVacancyController()
	r.Post("/search", h.VacancySearch)
	r.Get("/vacancy/{id}", h.VacancyGetByID)
	r.Get("/vacancies", h.VacancyGetList)
	r.Delete("/vacancy/{id}", h.VacancyDelete)

	//SwaggerUI
	r.Get("/swagger", router.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:         port,
		Handler:      r,
		ReadTimeout:  time.Second * 10,
		WriteTimeout: time.Second * 10,
	}

	// Webserver
	go func() {
		log.Printf(fmt.Sprintf("server started on port %s", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// waiting for the signal to terminate
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server...")

	// Timeout for finishing the job
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
