package controller

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-chi/chi"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"go-parser/models"
	"log"
	"net/http"
	"strconv"
	"unicode"
)

const maxTries = 5
const HabrCareerLink = "https://career.habr.com"

//const SeleniumURLPrefix = "http://vacancy_parserselenium:4444/wd/hub"

type VacancyServiceInterface interface {
	AddVacancy(json []byte) error
	DeleteVacancy(id string) error
	GetVacancy(id string) ([]byte, error)
	GetListVacancy() ([]byte, error)
}

type VacancyController struct {
	service     VacancyServiceInterface
	seleniumURL string
}

func NewVacancyController(service VacancyServiceInterface, seleniumURL string) *VacancyController {
	return &VacancyController{
		service:     service,
		seleniumURL: seleniumURL,
	}
}

func (v *VacancyController) VacancySearch(w http.ResponseWriter, r *http.Request) {
	var search models.Search
	err := json.NewDecoder(r.Body).Decode(&search) //get the query
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vacanciesList := findVacancies(search.Query, v.seleniumURL) // find links of vacancies

	for _, link := range vacanciesList { // get vacancies JSONs and add it to the data storage
		vacJSON := getVacancyJSON(link)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		_ = v.service.AddVacancy([]byte(vacJSON))
	}

}

func (v *VacancyController) VacancyGetByID(w http.ResponseWriter, r *http.Request) {
	var (
		vacancy      []byte
		vacancyIDRaw string
		err          error
	)

	vacancyIDRaw = chi.URLParam(r, "id")

	_, err = strconv.Atoi(vacancyIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vacancy, err = v.service.GetVacancy(vacancyIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	_, err = w.Write(vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) VacancyDelete(w http.ResponseWriter, r *http.Request) {
	var (
		vacIDRaw string
		err      error
	)

	vacIDRaw = chi.URLParam(r, "id")

	_, err = strconv.Atoi(vacIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = v.service.DeleteVacancy(vacIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}

	w.WriteHeader(200)
}

func (v *VacancyController) VacancyGetList(w http.ResponseWriter, r *http.Request) {
	vacList, err := v.service.GetListVacancy()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	_, err = w.Write(vacList)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func findVacancies(search string, seleniumURL string) []string {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	var err error
	// прописываем адрес нашего драйвера
	//urlPrefix := SeleniumURLPrefix
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, seleniumURL)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	// после окончания программы завершаем работу драйвера
	defer func(wd selenium.WebDriver) {
		err := wd.Quit()
		if err != nil {

		}
	}(wd)

	pages, err := getPages(wd)
	query := search // to get lots of vacancies
	var vacanciesLinks []string
	//var wg sync.WaitGroup
	//var mutex sync.Mutex

	for i := 1; i < pages+1; i++ {
		//wg.Add(1)
		//mutex.Lock()
		page := i // номер страницы
		//go func() {
		err = wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
		if err != nil {
			fmt.Println(err)
			wd.Quit()
			//return
		}
		links, err := getLinks(wd)
		if err != nil {
			fmt.Println(err)
			wd.Quit()
			//return
		}
		vacanciesLinks = append(vacanciesLinks, links...)
		//mutex.Unlock()
		//wg.Done()
		//}()
	}
	//go func() {
	//	wg.Wait()
	//}()

	return vacanciesLinks
}

func getPages(driver selenium.WebDriver) (int, error) {
	// сразу обращаемся к странице с поиском вакансии по запросу
	page := 1         // номер страницы
	query := "golang" // запрос
	err := driver.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
	if err != nil {
		return 0, err
	}
	elem, err := driver.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, err
	}
	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}

	var vacancies string
	for _, v := range vacancyCountRaw {
		if unicode.IsNumber(v) {
			vacancies += string(v)
		}
	}

	vacanciesCount, err := strconv.Atoi(vacancies)
	if err != nil {
		return 0, err
	}

	var pages int
	if vacanciesCount%25 == 0 { //25 vacancies per page
		pages = vacanciesCount / 25 // get the pages with the number of vacancies
	} else {
		pages = vacanciesCount/25 + 1
	}

	return pages, err
}

func getLinks(driver selenium.WebDriver) ([]string, error) {
	elems, err := driver.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return nil, err
	}
	var links []string
	for i := range elems {
		var link string
		link, err = elems[i].GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, HabrCareerLink+link)
	}

	return links, nil
}

func getVacancyJSON(link string) string {
	resp, err := http.Get(link)
	if err != nil {
		log.Println(err)
		return ""
	}
	var doc *goquery.Document
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil && doc != nil {
		log.Println(err)
		return ""
	}
	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		log.Println("habr vacancy nodes not found")
		return ""
	}
	ss := dd.First().Text()
	return ss
}
